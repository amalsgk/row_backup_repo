=== Seek ===

Contributors: ThemeInWP

Requires at least: 4.5
Tested up to: 5.2.2
Requires PHP: 5.5
Stable tag: 1.0.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A clean, elegant and professional WordPress Blog Theme.

== Description ==
Seek is a creative, clean, responsive and multi-layout highly organized WordPress magazine or news theme which is made to be used by online magazines, newspapers, editors, journalists, publishing units, bloggers, informative sites, educational websites, content writers, digital news media and other similar websites. Seek is optmized and cross browser compatible which will make your site attractive and eye catching in any browser and different variations of devices screen size.  Seek is build taking SEO, speed & content management in mind. Additional features from plugins like Booster Extension and Mailchimp Suscription will provide your content easy social share, as well as like-dislike option on direct connection to your user or reader. As Our theme is Comaptible with both old and new editor so you will not encounter any problem on setting your site up. Demo :https://demo.themeinwp.com/seek/


== Copyright ==

Seek WordPress Theme, Copyright 2019 ThemeInWP
Seek is distributed under the terms of the GNU GPL

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Credits ==

Underscores:
Author: 2012-2015 Automattic
Source: http://underscores.me
License: GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

normalize:
Author: 2012-2015 Nicolas Gallagher and Jonathan Neal
Source: http://necolas.github.io/normalize.css
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

BreadcrumbTrail:
Author: Justin Tadlock
Source: http://themehybrid.com/plugins/breadcrumb-trail
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Font Awesome:
Author: davegandy
Source: http://fontawesome.io
License: Licensed under the MIT license

Magnific 
Author: Chubby Ninja
Source: https://github.com/dimsemenov/Magnific-Popup
License: Licensed under the MIT license

Slick carousel:
Author: Ken Wheeler
Source: https://github.com/kenwheeler/slick
License: Licensed under the MIT license

theia-sticky-sidebar:
Author: Liviu Cristian Mirea Ghiban
Source: https://github.com/WeCodePixels/theia-sticky-sidebar/blob/master/LICENSE.txt
License: Licensed under the MIT license

== Image Used ==
https://stocksnap.io/photo/TE9BDY8EYB
https://stocksnap.io/photo/P7ZQ6D6Y8N
https://stocksnap.io/photo/HBA3AW23N8
https://stocksnap.io/photo/PFPBBV3HE8
https://stocksnap.io/photo/IFZYRGBZZS
https://stocksnap.io/photo/OLRVTGAAS2
https://stocksnap.io/photo/FRHAMZCZ2X
https://stocksnap.io/photo/5GPNLFT4ID
https://stocksnap.io/photo/RQV7KR0AAW


All Images are Licensed under CC0. https://creativecommons.org/publicdomain/zero/1.0/

== Google Fonts ==
Raleway
Source: https://fonts.google.com/specimen/raleway
Designer: Multiple Designers
License: Open Font License, Version 1.1.


== Changelog ==

= 1.0.0 - July 25 2019 =
* Initial release.

= 1.0.1 - July 27 2019 =
* Updated front page setting options.