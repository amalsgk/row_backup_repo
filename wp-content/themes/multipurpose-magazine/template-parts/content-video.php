<?php
/**
 * The template part for displaying post
 * @package Multipurpose Magazine
 * @subpackage multipurpose_magazine
 * @since 1.0
 */
?>
<?php
	$content = apply_filters( 'the_content', get_the_content() );
	$video = false;

	// Only get video from the content if a playlist isn't present.
	if ( false === strpos( $content, 'wp-playlist-script' ) ) {
		$video = get_media_embedded_in_content( $content, array( 'video', 'object', 'embed', 'iframe' ) );
	}
?>
<div class="blog-sec animated fadeInDown">
	<div class="mainimage">
		<?php
			if ( ! is_single() ) {
				// If not a single post, highlight the video file.
				if ( ! empty( $video ) ) {
					foreach ( $video as $video_html ) {
						echo '<div class="entry-video">';
							echo $video_html;
						echo '</div>';
					}
				};?>
				<div class="post-info">
			      <div class="dateday"><?php echo esc_html( get_the_date( 'd') ); ?></div>
			      <hr class="metahr m-0 p-0">
			      <div class="month"><?php echo esc_html( get_the_date( 'M' ) ); ?></div>
			      <div class="year"><?php echo esc_html( get_the_date( 'Y' ) ); ?></div>
			    </div>
			<?php }; 
		?> 
	</div>
	<h3><a href="<?php echo esc_url(get_permalink() ); ?>"><?php the_title(); ?></a></h3>
	<p><?php the_excerpt(); ?></p>
	<div class="blogbtn">
		<a href="<?php echo esc_url( get_permalink() );?>" class="blogbutton-small" title="<?php esc_attr_e( 'Read Full', 'multipurpose-magazine' ); ?>"><?php esc_html_e('Read Full','multipurpose-magazine'); ?></a>
	</div>
</div>