msgid ""
msgstr ""
"Project-Id-Version: Multipurpose Magazine \n"
"POT-Creation-Date: 2016-02-11 19:00+0530\n"
"PO-Revision-Date: 2016-02-11 19:00+0530\n"
"Last-Translator: \n"
"Language-Team: Themesglance <support@themesglance.com>\n"
"Language: English\n"
"MIME-Version: 0.1\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: 404.php:12
msgid "404 Not Found"
msgstr ""

#: 404.php:13
msgid "Looks like you have taken a wrong turn"
msgstr ""

#: 404.php:14
#: no-results.php:19
msgid "Dont worry it happens to the best of us."
msgstr ""

#: 404.php:16 
#: no-results.php:21
msgid "Return to the home page"
msgstr ""

#: comments.php:18
msgid "One thought on &ldquo;%s&rdquo;"
msgstr ""

#: comments.php:28
msgid "comments title"
msgstr ""

#: comments.php:58
msgid "'Comments are closed."
msgstr ""

#: no-results.php:9
msgid "Nothing Found"
msgstr ""

#: no-results.php:14
msgid "Ready to publish your first post? Get started here."
msgstr ""

#: no-results.php:16
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: footer.php:28
msgid "Copyright 2018"
msgstr ""

#: functions.php:47
msgid "Primary Menu"
msgstr ""

#: functions.php:80
msgid "Greetings from Themesglance!!"
msgstr ""

#: functions.php:81
msgid "A heartful thank you for choosing Themesglance. We promise to deliver only the best to you. Please proceed towards welcome section to get started."
msgstr ""

#: functions.php:82
msgid "About Theme"
msgstr ""

#: functions.php:89
msgid "Blog Sidebar"
msgstr ""

#: functions.php:90
msgid "Appears on blog page sidebar"
msgstr ""

#: functions.php:99
msgid "Page Sidebar"
msgstr ""

#: functions.php:100
#: functions.php:110
msgid "Appears on page sidebar"
msgstr ""

#: functions.php:109
msgid "Third Column Sidebar"
msgstr ""

#: functions.php:119
msgid "Footer 1"
msgstr ""

#: functions.php:129
msgid "Footer 2"
msgstr ""

#: functions.php:139
msgid "Footer 3"
msgstr ""

#: functions.php:149
msgid "Footer 4"
msgstr ""

#: functions.php:120
#: functions.php:130
#: functions.php:140
#: functions.php:150
msgid "Appears on footer"
msgstr ""

#: functions.php:159
msgid "Social Icon"
msgstr ""

#: functions.php:160
msgid "Appears on top bar"
msgstr ""

#: functions.php:169
msgid "Home Page Sidebar"
msgstr ""

#: functions.php:170
msgid "Appears on home page sidebar"
msgstr ""

#: functions.php:432
msgid "Magazine WordPress Theme"
msgstr ""

#: header.php:77
msgid "BUY NOW"
msgstr ""

#: header.php:151
msgid "BREAKING NEWS /"
msgstr ""

#: header.php:161
msgid "Menu"
msgstr ""

#: image.php:34
#: image.php:70
#: image.php:106
#: image.php:142
#: image.php:185
#: image.php:223
#: image.php:255
#: page.php:29
#: template-parts/single-post:25
#: page-template/page-with-left-sidebar.php:23
#: page-template/page-with-right-sidebar.php:19
msgid "Pages:"
msgstr ""

#: image.php:39
#: image.php:75
#: image.php:111
#: image.php:147
#: image.php:185
#: image.php:223
#: image.php:260
msgid "Edit"
msgstr ""

#: archive.php:33
#: archive.php:71
#: archive.php:104
#: archive.php:138
#: archive.php:175
#: archive.php:214
#: archive.php:251
#: index.php:29
#: index.php:63
#: index.php:92
#: index.php:122
#: index.php:155
#: index.php:188
#: index.php:217
#: search.php:30
#: search.php:65
#: search.php:95
#: search.php:126
#: search.php:160
#: search.php:196
#: search.php:230
msgid "Previous page"
msgstr ""

#: archive.php:34
#: archive.php:72
#: archive.php:105
#: archive.php:139
#: archive.php:176
#: archive.php:215
#: archive.php:252
#: index.php:30
#: index.php:64
#: index.php:93
#: index.php:123
#: index.php:156
#: index.php:189
#: index.php:218
#: search.php:31
#: search.php:66
#: search.php:96
#: search.php:127
#: search.php:161
#: search.php:197
#: search.php:231
msgid "Next page"
msgstr ""

#: archive.php:35
#: archive.php:73
#: archive.php:106
#: archive.php:140
#: archive.php:177
#: archive.php:216
#: archive.php:253
#: index.php:31
#: index.php:65
#: index.php:94
#: index.php:124
#: index.php:157
#: index.php:190
#: index.php:219
#: page.php:33
#: search.php:32
#: search.php:67
#: search.php:97
#: search.php:125
#: search.php:162
#: search.php:198
#: search.php:232
#: template-parts/single-post:29
#: page-template/page-with-left-sidebar.php:27
#: page-template/page-with-right-sidebar.php:23
msgid "page"
msgstr ""

#: search.php:16
#: search.php:51
#: search.php:81
#: search.php:112
#: search.php:146
#: search.php:180
#: search.php:216
msgid "Search Results For: %s"
msgstr ""

#: searchform.php:9
#: searchform.php:11
msgid "Search"
msgstr ""

#: searchform.php:9
msgid "placeholder"
msgstr ""

#: searchform.php:11
msgid "submit button"
msgstr ""

#: sidebar.php:10
msgid "Archives"
msgstr ""

#: sidebar.php:16
msgid "Meta"
msgstr ""

#: template-parts/single-post:36
msgid "Parent post link"
msgstr ""

#: template-parts/single-post:41
msgid "Next"
msgstr ""

#: template-parts/single-post:42
msgid "Next post:"
msgstr ""

#: template-parts/single-post:44
msgid "Previous"
msgstr ""

#: template-parts/single-post:45
msgid "Previous post:"
msgstr ""

#: template-parts/single-post:13
msgid "0 Comments"
msgstr ""

#: template-parts/content-audio.php:41
#: template-parts/content-gallery.php:29
#: template-parts/content-image.php:24
#: template-parts/content-video.php:42
#: template-parts/content.php:24
#: template-parts/grid-layout.php:25
msgid "Read Full"
msgstr ""

#: inc/customizer.php:15
msgid "Theme Settings"
msgstr ""

#: inc/customizer.php:16
msgid "Description of what this panel does."
msgstr ""

#: inc/customizer.php:116
msgid 'Typography'
msgstr ""

#: inc/customizer.php:127
msgid 'Paragraph Color'
msgstr ""

#: inc/customizer.php:141
msgid 'Paragraph Fonts'
msgstr ""

#: inc/customizer.php:152
msgid 'Paragraph Font Size'
msgstr ""

#: inc/customizer.php:164
msgid '"a" Tag Color'
msgstr ""

#: inc/customizer.php:178
msgid '"a" Tag Fonts'
msgstr ""

#: inc/customizer.php:189
msgid '"li" Tag Color'
msgstr ""

#: inc/customizer.php:203
msgid '"li" Tag Fonts'
msgstr ""

#: inc/customizer.php:214
msgid 'h1 Color'
msgstr ""

#: inc/customizer.php:228
msgid 'h1 Fonts'
msgstr ""

#: inc/customizer.php:240
msgid 'h1 Font Size'
msgstr ""

#: inc/customizer.php:252
msgid 'h2 Color'
msgstr ""

#: inc/customizer.php:266
msgid 'h2 Fonts'
msgstr ""

#: inc/customizer.php:278
msgid 'h2 Font Size'
msgstr ""

#: inc/customizer.php:290
msgid 'h3 Color'
msgstr ""

#: inc/customizer.php:304
msgid 'h3 Fonts'
msgstr ""

#: inc/customizer.php:316
msgid 'h3 Font Size'
msgstr ""

#: inc/customizer.php:328
msgid 'h4 Color'
msgstr ""

#: inc/customizer.php:342
msgid 'h4 Fonts'
msgstr ""

#: inc/customizer.php:354
msgid 'h4 Font Size'
msgstr ""

#: inc/customizer.php:366
msgid 'h5 Color'
msgstr ""

#: inc/customizer.php:380
msgid 'h5 Fonts'
msgstr ""

#: inc/customizer.php:392
msgid 'h5 Font Size'
msgstr ""

#: inc/customizer.php:404
msgid 'h6 Color'
msgstr ""

#: inc/customizer.php:418
msgid 'h6 Fonts'
msgstr ""

#: inc/customizer.php:430
msgid 'h6 Font Size'
msgstr ""

#: inc/customizer.php:439
msgid "Layout Settings"
msgstr ""

#: inc/customizer.php:446
msgid "Right Sidebar"
msgstr ""

#: inc/customizer.php:451
msgid "Select default layout"
msgstr ""

#: inc/customizer.php:466
msgid "Topbar Section"
msgstr ""

#: inc/customizer.php:467
msgid "Add Header Content here"
msgstr ""

#: inc/customizer.php:477
msgid "Select Popular Magazine Page"
msgstr ""

#: inc/customizer.php:478
msgid "Image Size ( 570 x 110 )"
msgstr ""

#: inc/customizer.php:488
msgid "Add Time"
msgstr ""

#: inc/customizer.php:499
msgid "Add Time Text"
msgstr ""

#: inc/customizer.php:510
msgid "Add Temperature"
msgstr ""

#: inc/customizer.php:521
msgid "Add Temperature Text"
msgstr ""

#: inc/customizer.php:532
msgid "Add Email Address"
msgstr ""

#: inc/customizer.php:543
msgid "Add Email Text"
msgstr ""

#: inc/customizer.php:554
msgid "Add Breaking News"
msgstr ""

#: inc/customizer.php:565
msgid "Add Login Text"
msgstr ""

#: inc/customizer.php:576
msgid "Add Login Link"
msgstr ""

#: inc/customizer.php:584
msgid "Slider Section"
msgstr ""

#: inc/customizer.php:610
msgid "Select Category to display Latest Post"
msgstr ""

#: inc/customizer.php:616
msgid "Top Trending News"
msgstr ""

#: inc/customizer.php:617
msgid "Add Top Trending sections below."
msgstr ""

#: inc/customizer.php:626
msgid "Section Title"
msgstr ""

#: inc/customizer.php:651
msgid "Select Category to display Latest Post"
msgstr ""

#: inc/customizer.php:657
msgid "Footer Text"
msgstr ""

#: inc/customizer.php:658
msgid "Add some text for footer like copyright etc."
msgstr ""

#: inc/customizer.php:667
msgid "Copyright Text"
msgstr ""

#: inc/customizer.php:783
msgid "Multipurpose Magazine Pro"
msgstr ""

#: inc/customizer.php:784
msgid "Go Pro"
msgstr ""